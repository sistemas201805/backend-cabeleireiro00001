using System;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using BuildingBlocks.Api.Core.Usuario;
using BuildingBlocks.Core.Messages.Integration;
using BuildingBlocks.Email.Services;

namespace Api.Services
{
    public class ConfirmarEmailService 
    {
        private readonly IAspNetUser _aspNetUser;
        private IEmailSender _emailSender;
        public ConfirmarEmailService(IEmailSender emailSender, 
                                     IAspNetUser aspNetUser)
        {
            _emailSender = emailSender;
            _aspNetUser = aspNetUser;
        }
        public async Task<ResponseMessage> SendConfirmarEmailAsync(string email, string userId, string code)
        {
            var callbackUrl = 
                $"{_aspNetUser.ObterHttpContext().Request.Scheme}://{_aspNetUser.ObterHttpContext().Request.Host}/api/v1/Identidade/Confirmar-Email?userId={userId}&code={code}";

            return await _emailSender.SendEmailAsync(email, "Confirme seu email", 
            $"Confirme seu endereço de e-mail <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicando aqui</a>."+
            "Se você não solicitou este e-mail, não se preocupe. Você pode ignorá-lo.");
        }
    }
}