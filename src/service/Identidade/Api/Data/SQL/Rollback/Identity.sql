﻿DROP TABLE "AspNetRoleClaims";

DROP TABLE "AspNetUserClaims";

DROP TABLE "AspNetUserLogins";

DROP TABLE "AspNetUserRoles";

DROP TABLE "AspNetUserTokens";

DROP TABLE "AspNetRoles";

DROP TABLE "AspNetUsers";

DROP TABLE "RefreshTokens";

DROP TABLE "SecurityKeys";

DELETE FROM "__EFMigrationsHistory"
WHERE "MigrationId" = '20200809133246_Inicial';

