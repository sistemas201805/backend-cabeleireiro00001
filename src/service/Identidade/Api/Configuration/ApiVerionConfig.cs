using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
 
namespace Api.Configuration
{
    public static class ApiVerionConfig
    {
        public static void AddApiVerionConfiguration(this IServiceCollection services)
        {
            services.AddApiVersioning(p =>
            {
                p.DefaultApiVersion = new ApiVersion(1, 0);
                p.ReportApiVersions = true;
                p.AssumeDefaultVersionWhenUnspecified = true;
            });
 
            services.AddVersionedApiExplorer(p =>
            {
                p.GroupNameFormat = "'v'VVV";
                p.SubstituteApiVersionInUrl = true;
            });
        }
 
        public static void UseApiVersionConfiguration(this IApplicationBuilder app)
        {
            app.UseApiVersioning();
        }
    }
}
