using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using BuildingBlocks.Api.Core.Usuario;
using BuildingBlocks.Core.Mediator;
using Api.Data;
using BuildingBlocks.Email.Services;
using Api.Services;
using Api.Data.Identity;

namespace Api.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            // Api
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAspNetUser, AspNetUser>();

            services.AddScoped<AuthenticationService>();            
            services.AddScoped<IEmailSender, EmailSender>();
            
            // Application
            services.AddScoped<IMediatorHandler, MediatorHandler>();

            services.AddScoped<ConfirmarEmailService>();

            // Data
            services.AddScoped<ApplicationDbContext>();
            services.AddScoped<AppIdentityDbContext>();            
        }
    }
}
