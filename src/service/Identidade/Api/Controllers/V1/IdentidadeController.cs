using System.Threading.Tasks;
using Api.Services;
using Api.ViewModels;
using AutoMapper;
using BuildingBlocks.Api.Core.Controllers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers.V1
{
    public class IdentidadeController: MainController
    {
        private readonly AuthenticationService _authenticationService;
        private readonly IMapper _mapper;

        public IdentidadeController(AuthenticationService authenticationService, 
                                    IMapper mapper)
        {
            _authenticationService = authenticationService;
            _mapper = mapper;
        }

        [HttpPost("nova-conta")]
        public async Task<ActionResult> Registrar(UsuarioRegistroViewModel usuarioRegistroViewModel, string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var user = _mapper.Map<IdentityUser>(usuarioRegistroViewModel);

            var result = await _authenticationService.CriarUsuario(user, usuarioRegistroViewModel.Senha);
            
            if (result.Succeeded)
            {
                var envioResult = await _authenticationService.EnviarConfirmarEmail(user);

                if (envioResult.ValidationResult.Errors.Count != 0) 
                {
                    await _authenticationService.RemoverUsuario(user);
                    return CustomResponse(envioResult.ValidationResult);
                }

                usuarioRegistroViewModel.Senha = "";
                usuarioRegistroViewModel.SenhaConfirmacao = "";
                return CustomResponse(usuarioRegistroViewModel);
            }

            foreach (var error in result.Errors)
            {
                AdicionarErroProcessamento(error.Description);
            }

            return CustomResponse();
        }

        [HttpGet("Confirmar-Email")]
        public async Task<IActionResult> ConfirmarEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                AdicionarErroProcessamento("Dados inválidos.");
                return CustomResponse();
            }

            var user = await _authenticationService.LocalizarUsuario(userId);
            if (user == null)
            {
                AdicionarErroProcessamento("Dados inválidos.");
                return CustomResponse();
            }            

            var result = await _authenticationService.ConfirmarEmail(user, code);
            if (!result.Succeeded) {
                AdicionarErroProcessamento("Falha ao confirmar seu e=mail.");
            } 
            return CustomResponse();
        }        

    }
}