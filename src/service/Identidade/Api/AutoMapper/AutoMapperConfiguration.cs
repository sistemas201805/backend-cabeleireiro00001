using Api.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;

namespace Api.AutoMapper
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration ()
        {
            CreateMap<IdentityUser, UsuarioRegistroViewModel>()
                .ForMember(c => c.Nome, opt => opt.MapFrom(src => src.UserName))
                .ForMember(c => c.Email, opt => opt.MapFrom(src => src.Email))
                .ReverseMap();
        }
    }
}