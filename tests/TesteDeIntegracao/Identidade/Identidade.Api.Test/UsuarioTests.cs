using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Api;
using Api.ViewModels;
using Identidade.Api.Test.Config;
using Identidade.Api.Test.Extensions;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace Identidade.Api.Test
{
    [Collection(nameof(IntegrationApiTestsFixtureCollection))]
    public class UsuarioTests
    {
        private readonly IntegrationTestsFixture<StartupTest> _testsFixture;
        public UsuarioTests(IntegrationTestsFixture<StartupTest> testsFixture)
        {
            _testsFixture = testsFixture;
        }

        [Fact(DisplayName = "Realizar cadastro com sucesso")]
        [Trait("Usuario", "Cadastro de usuário.")]
        public async Task Usuario_RealizarCadastro_ExecutarComSucesso()
        {
            // Arrange
            _testsFixture.GerarUserSenha();

            var email = "edson.tanaka1987@gmail.com"; //e-mail autorizado a enviar e receber na AWS 
            
            var usuario = new UsuarioRegistroViewModel()
            {
                Nome = email,
                Cpf = "36144890866",
                Email = email,
                Senha = _testsFixture.UsuarioSenha,
                SenhaConfirmacao = _testsFixture.UsuarioSenha 
            };

            // Act
            var postResponse = await _testsFixture.client.PostAsJsonAsync<UsuarioRegistroViewModel>("api/v1/Identidade/nova-conta", usuario);
            
            // Assert
            var Response = postResponse.Content.ReadAsStringAsync().Result;
            Assert.Contains(email, Response);
            postResponse.EnsureSuccessStatusCode();
        }
        
        [Fact(DisplayName = "Erros de dados inválidos ao tentar realizar o cadastro")]
        [Trait("Usuario", "Cadastro de usuário, erros de dados inválidos.")]
        public async Task Usuario_RealizarCadastro_ErrosDadosInvalidos()
        {
            // Arrange
            _testsFixture.GerarUserSenha();
            
            var usuario = new UsuarioRegistroViewModel()
            {
                Nome = _testsFixture.UsuarioEmail,
                Cpf = "11111111180",
                Email = "asdfa",
                Senha = "teste",
                SenhaConfirmacao = "teste" 
            };

            // Act
            var postResponse = await _testsFixture.client.PostAsJsonAsync<UsuarioRegistroViewModel>("api/v1/Identidade/nova-conta", usuario);
            
            // Assert
            Assert.Equal(postResponse.StatusCode, HttpStatusCode.BadRequest);
            var Response = postResponse.Content.ReadAsStringAsync().Result;
            Assert.Contains("Senha\":[\"O campo Senha precisa ter entre 6 e 100 caracteres\"]", Response);
            Assert.Contains("Email\":[\"O campo Email está em formato inválido\"]", Response);
        }
    }
}