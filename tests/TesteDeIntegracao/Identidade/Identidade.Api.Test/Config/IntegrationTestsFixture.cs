using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using Api;
using Bogus;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace Identidade.Api.Test.Config
{
    [CollectionDefinition(nameof(IntegrationApiTestsFixtureCollection))]
    public class IntegrationApiTestsFixtureCollection : ICollectionFixture<IntegrationTestsFixture<StartupTest>> { }
    public class IntegrationTestsFixture<TStartup> : IDisposable where TStartup : class
    {
        public string AntiForgeryFieldName = "__RequestVerificationToken";
        public string UsuarioEmail;
        public string UsuarioSenha;

        public string UsuarioToken;
        public readonly IdentidadeAppFactory<TStartup> factory;
        public HttpClient client;

        public IntegrationTestsFixture()
        {
            var clientOptions = new WebApplicationFactoryClientOptions()
            {
                AllowAutoRedirect = true,
                BaseAddress = new Uri("http://localhost"),
                HandleCookies = true,
                MaxAutomaticRedirections = 7
            };

            factory = new IdentidadeAppFactory<TStartup>();
            client = factory.CreateClient(clientOptions);

        }

        public void GerarUserSenha()
        {
            var faker = new Faker("pt_BR");
            UsuarioEmail = faker.Internet.Email().ToLower();
            UsuarioSenha = faker.Internet.Password(8, false, "", "@1Ab_");
        }

        public void Dispose()
        {
            client.Dispose();
            factory.Dispose();
        }
    }
}