﻿set ASPNETCORE_ENVIRONMENT=Development

set NetCoreVersion=3.1

set PortaDocker=5002
set PortaPostgresql=5432

set NomeProjeto=Cabeleireiro00001
set /p NomeServico=

IF NOT EXIST ./src (
    md src
)

cd src

  IF NOT EXIST ./BuildingBlocks (
    md BuildingBlocks
    cd BuildingBlocks
      git submodule add git@bitbucket.org:sistemas201805/BuildingBlocks.Api.Core.git
      git submodule add git@bitbucket.org:sistemas201805/BuildingBlocks.Core.git
      git submodule add git@bitbucket.org:sistemas201805/BuildingBlocks.MessageBus.git
      git submodule add git@bitbucket.org:sistemas201805/BuildingBlocks.Email.git
    cd..
  )

  IF NOT EXIST ./service (
    md service
  )

  cd service
    md %NomeServico%
    cd %NomeServico%

      dotnet new webApi --name Api --framework netcoreapp%NetCoreVersion%
      cd Api

        (
        echo ^<Project Sdk="Microsoft.NET.Sdk.Web"^>
        echo   ^<PropertyGroup^>
        echo     ^<TargetFramework^>netcoreapp3.1^<^/TargetFramework^>
        echo     ^<GenerateDocumentationFile^>true^<^/GenerateDocumentationFile^>
        echo     ^<NoWarn^>^$^(NoWarn^)^;1591^<^/NoWarn^>
        echo   ^<^/PropertyGroup^>
        echo ^<^/Project^>
        ) > Api.csproj

        dotnet user-secrets init

        dotnet add package AutoMapper
        dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection
        dotnet add package Microsoft.AspNetCore.Mvc.Versioning
        dotnet add package Microsoft.AspNetCore.Mvc.Versioning.ApiExplorer
        dotnet add package Microsoft.AspNetCore.Identity.EntityFrameworkCore
        dotnet add package Microsoft.EntityFrameworkCore.Design
        dotnet add package Microsoft.Extensions.Configuration.FileExtensions
        dotnet add package Microsoft.Extensions.Configuration.Json
        dotnet add package MediatR.Extensions.Microsoft.DependencyInjection
        dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL
        dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL.Design
        dotnet add package Swashbuckle.AspNetCore
        dotnet add Api.csproj reference ../../../BuildingBlocks/BuildingBlocks.Api.Core/BuildingBlocks.Api.Core.csproj
        dotnet add Api.csproj reference ../../../BuildingBlocks/BuildingBlocks.Core/BuildingBlocks.Core.csproj
        dotnet add Api.csproj reference ../../../BuildingBlocks/BuildingBlocks.MessageBus/BuildingBlocks.MessageBus.csproj
        dotnet add Api.csproj reference ../../../BuildingBlocks/BuildingBlocks.Email/BuildingBlocks.Email.csproj
        del WeatherForecast.cs

        del Startup.cs

        (
        echo using Api.Configuration;
        echo using AutoMapper;
        echo using BuildingBlocks.Api.Core.Identidade;
        echo using MediatR;
        echo using Microsoft.AspNetCore.Builder;
        echo using Microsoft.AspNetCore.Hosting;
        echo using Microsoft.AspNetCore.Mvc.ApiExplorer;
        echo using Microsoft.Extensions.Configuration;
        echo using Microsoft.Extensions.DependencyInjection;
        echo using Microsoft.Extensions.Hosting;
        echo.
        echo namespace Api
        echo {
        echo     public class Startup
        echo     {
        echo         public IConfiguration Configuration { get; }
        echo         public Startup^(IWebHostEnvironment env^)
        echo         {
        echo             var builder = new ConfigurationBuilder^(^)
        echo                 .SetBasePath^(env.ContentRootPath^)
        echo                 .AddJsonFile^("appsettings.json", true, true^)
        echo                 .AddJsonFile^($"appsettings.{env.EnvironmentName}.json", true, true^)
        echo                 .AddEnvironmentVariables^(^);
        echo.
        echo             if ^(env.IsDevelopment^(^)^)
        echo             {
        echo                 builder.AddUserSecrets^<Startup^>^(^);
        echo             }
        echo.
        echo             Configuration = builder.Build^(^);
        echo         }
        echo.
        echo         // This method gets called by the runtime. Use this method to add services to the container.
        echo         public void ConfigureServices^(IServiceCollection services^)
        echo         {
        echo             services.AddApiConfiguration^(Configuration^);
        echo.
        echo             services.AddJwtConfiguration^(Configuration^);
        echo.
        echo             services.AddApiVerionConfiguration^(^);
        echo.
        echo             services.AddAutoMapper^(typeof^(Startup^)^);
        echo.
        echo             services.AddSwaggerConfiguration^(^);
        echo.
        echo             services.AddMediatR^(typeof^(Startup^)^);
        echo.
        echo             services.RegisterServices^(^);
        echo.
        echo             services.AddMessageBusConfiguration^(Configuration^);
        echo         }
        echo.
        echo         // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        echo         public void Configure^(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider^)
        echo         {
        echo             app.UseApiVersionConfiguration^(^);
        echo.
        echo             app.UseSwaggerConfiguration^(provider^);
        echo.
        echo             app.UseApiConfiguration^(env^);
        echo         }
        echo     }
        echo }
        ) > Startup.cs

        del appsettings.Development.json

        (
        echo {
        echo   "ConnectionStrings": {
        echo     "DefaultConnection": "Host=localhost;Port=%PortaPostgresql%;Database=%NomeProjeto%;Username=postgres;Password=123"
        echo   },
        echo   "MessageQueueConnection": {
        echo     "MessageBus": "Host=localhost:5672;publisherConfirms=true;timeout=10"
        echo   },
        echo   "AppSettings": {
        echo     "Secret": "MEUSEGREDOSUPERSECRETO",
        echo     "ExpiracaoHoras": 2,
        echo     "Emissor": "Sistema%NomeProjeto%",
        echo     "ValidoEm": "https://localhost"
        echo   },
        echo   "Logging": {
        echo     "LogLevel": {
        echo       "Default": "Information",
        echo       "Microsoft": "Warning",
        echo       "Microsoft.Hosting.Lifetime": "Information"
        echo     }
        echo   }
        echo }
        ) > appsettings.Development.json

        (
        echo {
        echo   "ConnectionStrings": {
        echo     "DefaultConnection": "Host=localhost;Port=%PortaPostgresql%;Database=%NomeProjeto%;Username=postgres;Password=123"
        echo   },
        echo   "MessageQueueConnection": {
        echo     "MessageBus": "Host=localhost:5672;publisherConfirms=true;timeout=10"
        echo   },
        echo   "AppSettings": {
        echo     "Secret": "MEUSEGREDOSUPERSECRETO",
        echo     "ExpiracaoHoras": 2,
        echo     "Emissor": "Sistema%NomeProjeto%",
        echo     "ValidoEm": "https://localhost"
        echo   },
        echo   "Logging": {
        echo     "LogLevel": {
        echo       "Default": "Information",
        echo       "Microsoft": "Warning",
        echo       "Microsoft.Hosting.Lifetime": "Information"
        echo     }
        echo   }
        echo }
        ) > appsettings.Staging.json

        (
        echo FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS publishAspNetCore
        echo WORKDIR /src
        echo COPY Api.csproj /src
        echo RUN dotnet restore "/src/Api.csproj"
        echo COPY . .
        echo RUN dotnet publish "Api.csproj" -c Release -o /app/publish
        echo. 
        echo FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
        echo WORKDIR /app
        echo COPY --from=publishAspNetCore /app/publish .
        echo ENTRYPOINT ["dotnet", "Api.dll"]
        ) > DockerFile

        md Models
        md ViewModels
        md Application
        cd Application
          md Commands
          md Events
          md Queries
        cd..
        
        md Data
        cd Data
          md Repository
          md Mappings

          (
          echo using FluentValidation.Results;
          echo using Microsoft.EntityFrameworkCore;
          echo using BuildingBlocks.Core.Mediator;
          echo using BuildingBlocks.Core.Messages;
          echo using System;
          echo using System.Linq;
          echo using System.Threading.Tasks;
          echo using BuildingBlocks.Core.Data;
          echo using Api.Extensions;
          echo.
          echo namespace Api.Data
          echo {
          echo     public class ApplicationDbContext : DbContext, IUnitOfWork
          echo     {
          echo         private readonly IMediatorHandler _mediatorHandler;
          echo         public ApplicationDbContext^(DbContextOptions^<ApplicationDbContext^> options, IMediatorHandler mediatorHandler^) 
          echo             : base^(options^) 
          echo         {
          echo             _mediatorHandler = mediatorHandler;
          echo         }
          echo.
          echo         protected override void OnModelCreating^(ModelBuilder modelBuilder^)
          echo         { 
          echo             foreach ^(var property in modelBuilder.Model.GetEntityTypes^(^).SelectMany^(
          echo                 e =^> e.GetProperties^(^).Where^(p =^> p.ClrType =^= typeof^(string^)^)^)^)
          echo                 property.SetColumnType^("varchar^(100^)"^);
          echo.
          echo             modelBuilder.Ignore^<Event^>^(^);
          echo             modelBuilder.Ignore^<ValidationResult^>^(^);
          echo.
          echo             modelBuilder.ApplyConfigurationsFromAssembly^(typeof^(ApplicationDbContext^).Assembly^);
          echo.
          echo             foreach ^(var relationship in modelBuilder.Model.GetEntityTypes^(^)
          echo                 .SelectMany^(e =^> e.GetForeignKeys^(^)^)^) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;
          echo.        
          echo             base.OnModelCreating^(modelBuilder^);
          echo         }
          echo.
          echo         public async Task^<bool^> Commit^(^)
          echo         {
          echo             foreach ^(var entry in ChangeTracker.Entries^(^)
          echo                 .Where^(entry =^> entry.Entity.GetType^(^).GetProperty^("DataCadastro"^) != null^)^)
          echo             {
          echo                 if ^(entry.State == EntityState.Added^)
          echo                 {
          echo                     entry.Property^("DataCadastro"^).CurrentValue = DateTime.Now;
          echo                 }
          echo.
          echo                 if ^(entry.State == EntityState.Modified^)
          echo                 {
          echo                     entry.Property^("DataCadastro"^).IsModified = false;
          echo                 }
          echo             }
          echo.
          echo             var sucesso = await base.SaveChangesAsync^(^) ^> 0;
          echo             if ^(sucesso^) await _mediatorHandler.PublicarEventos^(this^);
          echo.
          echo             return sucesso;
          echo         }
          echo     }
          echo }
          ) > ApplicationDbContext.cs

        cd..

        md Configuration
        cd Configuration

          (
          echo using Microsoft.AspNetCore.Builder;
          echo using Microsoft.AspNetCore.Hosting;
          echo using Microsoft.EntityFrameworkCore;
          echo using Microsoft.Extensions.Configuration;
          echo using Microsoft.Extensions.DependencyInjection;
          echo using Microsoft.Extensions.Hosting;
          echo using Api.Data;
          echo using BuildingBlocks.Api.Core.Identidade;
          echo. 
          echo namespace Api.Configuration
          echo {
          echo     public static class ApiConfig
          echo     {
          echo         public static void AddApiConfiguration^(this IServiceCollection services, IConfiguration configuration^)
          echo         {
          echo             services.AddDbContext^<ApplicationDbContext^>^(options =^>
          echo                 options.UseNpgsql^(configuration.GetConnectionString^("DefaultConnection"^)^)^);
          echo. 
          echo             services.AddControllers^(^);
          echo. 
          echo             services.AddCors^(options =^>
          echo             {
          echo                 options.AddPolicy^("Total",
          echo                     builder =^>
          echo                         builder
          echo                             .AllowAnyOrigin^(^)
          echo                             .AllowAnyMethod^(^)
          echo                             .AllowAnyHeader^(^)^);
          echo             }^);
          echo         }
          echo. 
          echo         public static void UseApiConfiguration^(this IApplicationBuilder app, IWebHostEnvironment env^)
          echo         {
          echo             if ^(env.IsDevelopment^(^)^)
          echo             {
          echo                 app.UseDeveloperExceptionPage^(^);
          echo             }
          echo. 
          echo             app.UseHttpsRedirection^(^);
          echo. 
          echo             app.UseRouting^(^);
          echo. 
          echo             app.UseCors^("Total"^);
          echo. 
          echo             app.UseAuthConfiguration^(^);
          echo. 
          echo             app.UseEndpoints^(endpoints =^>
          echo             {
          echo                 endpoints.MapControllers^(^);
          echo             }^);
          echo         }
          echo     }
          echo }
          ) > ApiConfig.cs

          (
          echo using AutoMapper;
          echo.
          echo namespace Api.Configuration
          echo {
          echo     public class AutomapperConfig : Profile
          echo     {
          echo         public AutomapperConfig^(^)
          echo         {
          echo         }
          echo     }
          echo }
          ) > AutomapperConfig.cs

          (
          echo using Microsoft.AspNetCore.Http;
          echo using Microsoft.Extensions.DependencyInjection;
          echo using BuildingBlocks.Api.Core.Usuario;
          echo using BuildingBlocks.Core.Mediator;
          echo using Api.Data;
          echo. 
          echo namespace Api.Configuration
          echo {
          echo     public static class DependencyInjectionConfig
          echo     {
          echo         public static void RegisterServices^(this IServiceCollection services^)
          echo         {
          echo             // Api
          echo             services.AddSingleton^<IHttpContextAccessor, HttpContextAccessor^>^(^);
          echo             services.AddScoped^<IAspNetUser, AspNetUser^>^(^);
          echo.
          echo             // Application
          echo             services.AddScoped^<IMediatorHandler, MediatorHandler^>^(^);
          echo.
          echo             // Data
          echo             services.AddScoped^<ApplicationDbContext^>^(^);
          echo         }
          echo     }
          echo }
          ) > DependencyInjectionConfig.cs

          (
          echo using Microsoft.Extensions.Configuration;
          echo using Microsoft.Extensions.DependencyInjection;
          echo using BuildingBlocks.Core.Utils;
          echo using BuildingBlocks.MessageBus;
          echo.
          echo namespace Api.Configuration
          echo {
          echo     public static class MessageBusConfig
          echo     {
          echo         public static void AddMessageBusConfiguration^(this IServiceCollection services,
          echo             IConfiguration configuration^)
          echo         {
          echo             services.AddMessageBus^(configuration.GetMessageQueueConnection^("MessageBus"^)^);
          echo         }
          echo     }
          echo }
          ) > MessageBusConfig.cs
          

          (
          echo using System;
          echo using System.IO;
          echo using System.Reflection;
          echo using Microsoft.AspNetCore.Builder;
          echo using Microsoft.AspNetCore.Mvc.ApiExplorer;
          echo using Microsoft.Extensions.DependencyInjection;
          echo using Microsoft.OpenApi.Models;
          echo. 
          echo namespace Api.Configuration
          echo {
          echo     public static class SwaggerConfig
          echo     {
          echo         public static void AddSwaggerConfiguration^(this IServiceCollection services^)
          echo         {
          echo             services.AddSwaggerGen^(c =^>
          echo             {
          echo                 var provider = services.BuildServiceProvider^(^).GetRequiredService^<IApiVersionDescriptionProvider^>^(^);
          echo. 
          echo                 foreach ^(var description in provider.ApiVersionDescriptions^)
          echo                 {
          echo                     c.SwaggerDoc^(description.GroupName, CreateInfoForApiVersion^(description^)^);
          echo                 }
          echo. 
          echo                 c.AddSecurityDefinition^("Bearer", new OpenApiSecurityScheme
          echo                 {
          echo                     Description = "Insira o Token JWT desta maneira: Bearer {token}",
          echo                     Name = "Authorization",
          echo                     Scheme = "Bearer",
          echo                     BearerFormat = "JWT",
          echo                     In = ParameterLocation.Header,
          echo                     Type = SecuritySchemeType.ApiKey
          echo                 }^);
          echo. 
          echo                 c.AddSecurityRequirement^(new OpenApiSecurityRequirement
          echo                 {
          echo                     {
          echo                         new OpenApiSecurityScheme
          echo                         {
          echo                             Reference = new OpenApiReference
          echo                             {
          echo                                 Type = ReferenceType.SecurityScheme,
          echo                                 Id = "Bearer"
          echo                             }
          echo                         },
          echo                         new string[] { }
          echo                     }
          echo                 }^);
          echo. 
          echo                 c.IncludeXmlComments^(XmlCommentsFilePath^);
          echo             }^);
          echo         }
          echo. 
          echo         private static string XmlCommentsFilePath
          echo         {
          echo             get
          echo             {
          echo                 var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
          echo                 return Path.Combine^(AppContext.BaseDirectory, xmlFile^);
          echo             }
          echo         }
          echo. 
          echo         private static OpenApiInfo CreateInfoForApiVersion^(ApiVersionDescription description^)
          echo         {
          echo             var info = new OpenApiInfo^(^)
          echo             {
          echo                 Title = $"%NomeProjeto% %NomeServico% Api {description.ApiVersion}",
          echo                 Version = description.ApiVersion.ToString^(^),
          echo                 Description = "Esta Api faz parte do sistema %NomeProjeto%.",
          echo                 Contact = new OpenApiContact^(^) { Name = "Edson Tanaka", Email = "edson.tanaka1987@gmail.com" }
          echo             };
          echo             if ^(description.IsDeprecated^)
          echo             {
          echo                 info.Description += " Esta versão da Api foi descontinuada. ";
          echo             }
          echo             return info;
          echo         }
          echo. 
          echo         public static void UseSwaggerConfiguration^(this IApplicationBuilder app, IApiVersionDescriptionProvider provider^)
          echo         {
          echo             app.UseSwagger^(^);
          echo             app.UseSwaggerUI^(c =^>
          echo             {
          echo                 foreach ^(var description in provider.ApiVersionDescriptions^)
          echo                 {
          echo                     c.SwaggerEndpoint^(
          echo                     $"/swagger/{description.GroupName}/swagger.json",
          echo                     description.GroupName.ToUpperInvariant^(^)^);
          echo                 }
          echo             }^);
          echo         }
          echo     }
          echo }
          ) > SwaggerConfig.cs

          (
          echo using Microsoft.AspNetCore.Builder;
          echo using Microsoft.AspNetCore.Mvc;
          echo using Microsoft.Extensions.DependencyInjection;
          echo. 
          echo namespace Api.Configuration
          echo {
          echo     public static class ApiVerionConfig
          echo     {
          echo         public static void AddApiVerionConfiguration^(this IServiceCollection services^)
          echo         {
          echo             services.AddApiVersioning^(p =^>
          echo             {
          echo                 p.DefaultApiVersion = new ApiVersion^(1, 0^);
          echo                 p.ReportApiVersions = true;
          echo                 p.AssumeDefaultVersionWhenUnspecified = true;
          echo             }^);
          echo. 
          echo             services.AddVersionedApiExplorer^(p =^>
          echo             {
          echo                 p.GroupNameFormat = "'v'VVV";
          echo                 p.SubstituteApiVersionInUrl = true;
          echo             }^);
          echo         }
          echo. 
          echo         public static void UseApiVersionConfiguration^(this IApplicationBuilder app^)
          echo         {
          echo             app.UseApiVersioning^(^);
          echo         }
          echo     }
          echo }
          ) > ApiVerionConfig.cs

        cd..
        cd Controllers
          md V1
          del WeatherForecastController.cs
        cd..

        md Extensions
        cd Extensions
          (
          echo using Microsoft.EntityFrameworkCore;
          echo using BuildingBlocks.Core.DomainObject;
          echo using BuildingBlocks.Core.Mediator;
          echo using System.Linq;
          echo using System.Threading.Tasks;
          echo. 
          echo namespace Api.Extensions
          echo {
          echo     public static class MediatorExtension
          echo     {
          echo         public static async Task PublicarEventos^<T^>^(this IMediatorHandler mediator, T ctx^) where T : DbContext
          echo         {
          echo             var domainEntities = ctx.ChangeTracker
          echo                 .Entries^<Entity^>^(^)
          echo                 .Where^(x =^> x.Entity.Notificacoes != null ^&^& x.Entity.Notificacoes.Any^(^)^);
          echo. 
          echo             var domainEvents = domainEntities
          echo                 .SelectMany^(x =^> x.Entity.Notificacoes^)
          echo                 .ToList^(^);
          echo. 
          echo             domainEntities.ToList^(^)
          echo                 .ForEach^(entity =^> entity.Entity.LimparEventos^(^)^);
          echo. 
          echo             var tasks = domainEvents
          echo                 .Select^(async ^(domainEvent^) =^>
          echo                 {
          echo                     await mediator.PublicarEvento^(domainEvent^);
          echo                 }^);
          echo. 
          echo             await Task.WhenAll^(tasks^);
          echo         }
          echo     }
          echo }
          ) > MediatorExtension.cs
        cd..
      cd..
    cd..
  cd..
cd..


IF NOT EXIST ./test (
    md test
)

cd test

  IF NOT EXIST ./TesteDeUnidade (
    md TesteDeUnidade
  )

  cd TesteDeUnidade
    md %NomeServico%
    cd %NomeServico%
      dotnet new xunit --name Api --framework netcoreapp%NetCoreVersion%
      cd Api
        del UnitTest1.cs
      cd..
      dotnet new xunit --name Domain --framework netcoreapp%NetCoreVersion%
      cd Domain
        del UnitTest1.cs
      cd..
      dotnet new xunit --name Infra.Data --framework netcoreapp%NetCoreVersion%
      cd Infra.Data
        del UnitTest1.cs
      cd..
    cd..
  cd..

  IF NOT EXIST ./TesteDeIntegracao (
    md TesteDeIntegracao
  )

  cd TesteDeIntegracao
    md %NomeServico%
    cd %NomeServico%
      dotnet new xunit --name Api --framework netcoreapp%NetCoreVersion%
      cd Api
        del UnitTest1.cs
      cd..
    cd..
  cd..
cd..

IF NOT EXIST .gitignore (
  (
  echo \bin
  echo \obj
  ) > .gitignore
)

IF NOT EXIST .dockerignore (
  (
  echo **/.classpath
  echo **/.dockerignore
  echo **/.env
  echo **/.git
  echo **/.gitignore
  echo **/.project
  echo **/.settings
  echo **/.toolstarget
  echo **/.vs
  echo **/.vscode
  echo **/*.*proj.user
  echo **/*.dbmdl
  echo **/*.jfm
  echo **/azds.yaml
  echo **/bin
  echo **/charts
  echo **/docker-compose*
  echo **/Dockerfile*
  echo **/node_modules
  echo **/npm-debug.log
  echo **/obj
  echo **/secrets.dev.yaml
  echo **/values.dev.yaml
  echo README.md
  ) > .dockerignore
)

IF NOT EXIST docker-compose.yml (
  (
  echo version: '3.4'
  echo. 
  echo services:
  echo   %NomeServico%:
  echo     image: %NomeServico%
  echo     build:
  echo       context: .
  echo       dockerfile: src/service/%NomeServico%/Api/Dockerfile
  echo     ports:
  echo       - %PortaDocker%
  echo     environment: 
  echo       - ASPNETCORE_ENVIRONMENT=Development
  ) > docker-compose.yml
)